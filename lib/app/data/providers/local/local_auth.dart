//import 'dart:convert';

import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get/get.dart';
//import 'package:getx_pattern/app/data/models/request_token.dart';

class LocalAuth {
  //Clase q expone metodos get y set q interactuan con el Flutter Secure Storage
  //static final KEY = "session";
  final FlutterSecureStorage _storage = Get.find<FlutterSecureStorage>();

  /*Future<void> setSession(RequestToken requestToken) async {
    await _storage.write(key: KEY, value: jsonEncode(requestToken.toJson()));
  }*/

  /*Future<RequestToken> getSession() async {
    final String data = await _storage.read(key: KEY);
    if (data != null) {
      final RequestToken requestToken = RequestToken.fromJson(jsonDecode(data));

      if (DateTime.now().isBefore(requestToken.expiresAt)) {
        return requestToken;
      }

      return null;
    }
    return null;
  }*/

  //-----------------------------------------------------------
  Future<void> setRegistrado(bool valor) async {
    await _storage.write(key: "ISREGISTERED", value: valor.toString());
  }

  Future<bool> getRegistrado() async {
    final String? data = await _storage.read(key: "ISREGISTERED");

    if (data == "true") return true;

    return false;
  }

  Future<void> setSession(bool valor) async {
    await _storage.write(key: "AUTHENTICATED", value: valor.toString());
  }

  Future<bool> getSession() async {
    final String? data = await _storage.read(key: "AUTHENTICATED");

    if (data == "true") return true;

    return false;
  }
}
