import 'package:dio/dio.dart';
import 'package:food_trady/app/data/models/categoria.dart';
import 'package:food_trady/app/data/models/cliente-registro.dart';
import 'package:food_trady/app/data/models/cliente.dart';
import 'package:food_trady/app/data/models/multimedia.dart';
import 'package:food_trady/app/data/models/token.dart';
import 'package:food_trady/app/utils/diobase.dart';
import 'package:food_trady/app/utils/vuex.dart';
import 'package:get/get.dart' as GET;

class ProviderApi {
  //busca depenedecia en el entorno que ha sido inyectada en el main
  //final Dio _dio = GET.Get.find<Dio>();
  final DioBase _dio = GET.Get.find<DioBase>();

  //Permite obtener un token para utilizarlo en las posteriores llamadas al api de selfservice
  Future<Token> newToken(
      {required String identifier, required String password}) async {
    final Response response = await _dio.dioService().post('/auth/local',
        data: {"identifier": identifier, "password": password});

    return Token.fromJson(response.data);
  }

  Future<List<Categorias>> categorias() async {
    String myToken = Vuex.myToken;

    final Response response = await _dio.dioService().get('/categorias',
        options: Options(
          headers: {'Authorization': 'Bearer $myToken'},
        ));

    return (response.data as List).map((e) => Categorias.fromJson(e)).toList();
  }

  Future<List<Multimedias>> multimedias(String modulo) async {
    String myToken = Vuex.myToken;

    final Response response = await _dio
        .dioService()
        .get('/multimedias?tipo_multimedia.Modulo=$modulo&_sort=Orden:ASC',
            options: Options(
              headers: {'Authorization': 'Bearer $myToken'},
            ));

    return (response.data as List).map((e) => Multimedias.fromJson(e)).toList();
  }

  Future<Cliente> register(
      {required String telefono,
      required String correo,
      required String smsCode}) async {
    String myToken = Vuex.myToken;

    final Response response = await _dio.dioService().post('/clientes',
        data: {
          "Nombre": "",
          "Correo": correo,
          "Telefono": telefono,
          "Estatus": 1,
          "Verificado": true
        },
        options: Options(
          headers: {'Authorization': 'Bearer $myToken'},
        ));

    return Cliente.fromJson(response.data);
  }

  Future<ClienteRegistro> clienteRegistro(
      {required String correo,
      required String telefono,
      required String clave}) async {
    String myToken = Vuex.myToken;

    final Response response = await _dio.dioService().post('/registrar-cliente',
        data: {
          "correo": correo,
          "telefono": telefono,
          "clave": clave,
        },
        options: Options(
          headers: {'Authorization': 'Bearer $myToken'},
        ));

    return ClienteRegistro.fromJson(response.data);
  }

  Future<ClienteRegistro> validaTelefono({required String telefono}) async {
    String myToken = Vuex.myToken;

    final Response response =
        await _dio.dioService().post('/cliente-por-telefono',
            data: {
              "telefono": telefono,
            },
            options: Options(
              headers: {'Authorization': 'Bearer $myToken'},
            ));

    return ClienteRegistro.fromJson(response.data);
  }

  //
  Future<ClienteRegistro> clienteLogin(
      {required String correo, required String clave}) async {
    String myToken = Vuex.myToken;

    final Response response = await _dio.dioService().post('/login-cliente',
        data: {
          "correo": correo,
          "clave": clave,
        },
        options: Options(
          headers: {'Authorization': 'Bearer $myToken'},
        ));

    return ClienteRegistro.fromJson(response.data);
  }
}
