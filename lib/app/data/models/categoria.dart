class Categorias {
  Categorias({
    required this.id,
    required this.estatus,
    required this.publishedAt,
    required this.createdAt,
    required this.updatedAt,
    required this.v,
    required this.descripcionCorta,
    required this.descripcionLarga,
    required this.multimedia,
    required this.categoriaId,
  });

  final String id;
  final int estatus;
  final DateTime publishedAt;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int v;
  final String descripcionCorta;
  final String descripcionLarga;
  final Multimedia multimedia;
  final String categoriaId;

  factory Categorias.fromJson(Map<String, dynamic> json) => Categorias(
        id: json["_id"],
        estatus: json["Estatus"],
        publishedAt: DateTime.parse(json["published_at"]),
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        v: json["__v"],
        descripcionCorta: json["DescripcionCorta"],
        descripcionLarga: json["DescripcionLarga"],
        multimedia: Multimedia.fromJson(json["multimedia"]),
        categoriaId: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "Estatus": estatus,
        "published_at": publishedAt.toIso8601String(),
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "__v": v,
        "DescripcionCorta": descripcionCorta,
        "DescripcionLarga": descripcionLarga,
        "multimedia": multimedia.toJson(),
        "id": categoriaId,
      };
}

class Multimedia {
  Multimedia({
    required this.id,
    required this.asset,
    required this.tipo,
    required this.estatus,
    required this.nombre,
    required this.publishedAt,
    required this.createdAt,
    required this.updatedAt,
    required this.v,
    required this.multimediaId,
  });

  final String id;
  final String asset;
  final int tipo;
  final int estatus;
  final String nombre;
  final DateTime publishedAt;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int v;
  final String multimediaId;

  factory Multimedia.fromJson(Map<String, dynamic> json) => Multimedia(
        id: json["_id"],
        asset: json["Asset"],
        tipo: json["tipo"],
        estatus: json["estatus"],
        nombre: json["nombre"],
        publishedAt: DateTime.parse(json["published_at"]),
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        v: json["__v"],
        multimediaId: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "Asset": asset,
        "tipo": tipo,
        "estatus": estatus,
        "nombre": nombre,
        "published_at": publishedAt.toIso8601String(),
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "__v": v,
        "id": multimediaId,
      };
}
