// To parse this JSON data, do
//
//     final token = tokenFromJson(jsonString);

//Token tokenFromJson(String str) => Token.fromJson(json.decode(str));

//String tokenToJson(Token data) => json.encode(data.toJson());

class Token {
  Token({
    required this.jwt,
    required this.user,
  });

  final String jwt;
  final User user;

  factory Token.fromJson(Map<String, dynamic> json) => Token(
        jwt: json["jwt"],
        user: User.fromJson(json["user"]),
      );

  Map<String, dynamic> toJson() => {
        "jwt": jwt,
        "user": user.toJson(),
      };
}

class User {
  User({
    required this.confirmed,
    required this.blocked,
    required this.id,
    required this.username,
    required this.email,
    required this.provider,
    required this.createdAt,
    required this.updatedAt,
    required this.v,
    required this.role,
    required this.userId,
  });

  final bool confirmed;
  final bool blocked;
  final String id;
  final String username;
  final String email;
  final String provider;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int v;
  final Role role;
  final String userId;

  factory User.fromJson(Map<String, dynamic> json) => User(
        confirmed: json["confirmed"],
        blocked: json["blocked"],
        id: json["_id"],
        username: json["username"],
        email: json["email"],
        provider: json["provider"],
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        v: json["__v"],
        role: Role.fromJson(json["role"]),
        userId: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "confirmed": confirmed,
        "blocked": blocked,
        "_id": id,
        "username": username,
        "email": email,
        "provider": provider,
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "__v": v,
        "role": role.toJson(),
        "id": userId,
      };
}

class Role {
  Role({
    required this.id,
    required this.name,
    required this.description,
    required this.type,
    required this.v,
    required this.roleId,
  });

  final String id;
  final String name;
  final String description;
  final String type;
  final int v;
  final String roleId;

  factory Role.fromJson(Map<String, dynamic> json) => Role(
        id: json["_id"],
        name: json["name"],
        description: json["description"],
        type: json["type"],
        v: json["__v"],
        roleId: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "name": name,
        "description": description,
        "type": type,
        "__v": v,
        "id": roleId,
      };
}
