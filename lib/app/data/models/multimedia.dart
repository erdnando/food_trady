class Multimedias {
  Multimedias({
    required this.id,
    required this.asset,
    required this.tipo,
    required this.estatus,
    required this.publishedAt,
    required this.createdAt,
    required this.updatedAt,
    required this.v,
    required this.nombre,
    required this.tipoMultimedia,
    required this.orden,
    required this.multimediaId,
    required this.descripcion,
  });

  final String id;
  final String asset;
  final int tipo;
  final int estatus;
  final DateTime publishedAt;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int v;
  final String nombre;
  final TipoMultimedia tipoMultimedia;
  final int orden;
  final String multimediaId;
  final String descripcion;

  factory Multimedias.fromJson(Map<String, dynamic> json) => Multimedias(
        id: json["_id"],
        asset: json["Asset"],
        tipo: json["tipo"] == null ? 0 : json["tipo"],
        estatus: json["estatus"],
        publishedAt: DateTime.parse(json["published_at"]),
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        v: json["__v"],
        nombre: json["nombre"],
        tipoMultimedia: TipoMultimedia.fromJson(json["tipo_multimedia"]),
        orden: json["Orden"],
        multimediaId: json["id"],
        descripcion: json["Descripcion"] == null ? "" : json["Descripcion"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "Asset": asset,
        "tipo": tipo == 0 ? null : tipo,
        "estatus": estatus,
        "published_at": publishedAt.toIso8601String(),
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "__v": v,
        "nombre": nombre,
        "tipo_multimedia": tipoMultimedia.toJson(),
        "Orden": orden,
        "id": multimediaId,
        "Descripcion": descripcion == "" ? null : descripcion,
      };
}

class TipoMultimedia {
  TipoMultimedia({
    required this.id,
    required this.modulo,
    required this.estatus,
    required this.publishedAt,
    required this.createdAt,
    required this.updatedAt,
    required this.v,
    required this.tipoMultimediaId,
  });

  final String id;
  final String modulo;
  final int estatus;
  final DateTime publishedAt;
  final DateTime createdAt;
  final DateTime updatedAt;
  final int v;
  final String tipoMultimediaId;

  factory TipoMultimedia.fromJson(Map<String, dynamic> json) => TipoMultimedia(
        id: json["_id"],
        modulo: json["Modulo"],
        estatus: json["Estatus"],
        publishedAt: DateTime.parse(json["published_at"]),
        createdAt: DateTime.parse(json["createdAt"]),
        updatedAt: DateTime.parse(json["updatedAt"]),
        v: json["__v"],
        tipoMultimediaId: json["id"],
      );

  Map<String, dynamic> toJson() => {
        "_id": id,
        "Modulo": modulo,
        "Estatus": estatus,
        "published_at": publishedAt.toIso8601String(),
        "createdAt": createdAt.toIso8601String(),
        "updatedAt": updatedAt.toIso8601String(),
        "__v": v,
        "id": tipoMultimediaId,
      };
}
