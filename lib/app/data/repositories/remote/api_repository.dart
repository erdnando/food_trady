import 'package:food_trady/app/data/models/categoria.dart';
import 'package:food_trady/app/data/models/cliente-registro.dart';
import 'package:food_trady/app/data/models/cliente.dart';
import 'package:food_trady/app/data/models/multimedia.dart';
import 'package:food_trady/app/data/models/token.dart';
import 'package:food_trady/app/data/providers/remote/provider_api.dart';
import 'package:get/utils.dart';
import 'package:get/get.dart';
//import 'package:meta/meta.dart' show required;

class ApiRepository {
  //busca depenedecia en el entorno que ha sido inyectada en el main
  final ProviderApi _api = Get.find<ProviderApi>();

  Future<Token> newToken(
      {required String identifier, required String password}) {
    return _api.newToken(identifier: identifier, password: password);
  }

  Future<List<Categorias>> categorias() {
    return _api.categorias();
  }

  Future<List<Multimedias>> multimedias(String modulo) {
    return _api.multimedias(modulo);
  }

  Future<Cliente> register(
      {required String telefono,
      required String correo,
      required String smsCode}) {
    return _api.register(telefono: telefono, correo: correo, smsCode: smsCode);
  }

  Future<ClienteRegistro> clienteRegistro(
      {required String correo,
      required String telefono,
      required String clave}) {
    return _api.clienteRegistro(
        correo: correo, telefono: telefono, clave: clave);
  }

  Future<ClienteRegistro> validaTelefono({required String telefono}) {
    return _api.validaTelefono(telefono: telefono);
  }

  Future<ClienteRegistro> clienteLogin(
      {required String correo, required String clave}) {
    return _api.clienteLogin(correo: correo, clave: clave);
  }
}
