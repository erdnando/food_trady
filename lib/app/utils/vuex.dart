import 'package:flutter/material.dart';
import 'package:food_trady/app/modules/home/tabs/productos/local_widgets/modal_detalle_producto.dart';
import 'package:food_trady/app/modules/home/tabs/productos/productos_controller.dart';
import 'package:food_trady/app/routes/app_routes.dart';
import 'package:get/get.dart';

class Vuex {
//--------------------------------------------------------------------------------------------------------------------------------
  //Variables globales
  //static String TOKEN_GENERADO = '';
  //static String THE_MOVIE_DB_API_KEY = "0bd141a296b8758dbf4de1e8c0b0b469";
  static String myToken = '';
  static String identifier = 'system@selfservice.com';
  static String password = 'Welcome\$1';
  static String contentType = 'application/json; charset=utf-8';
  static bool loading = true;

  static bool cameFromRegister = false;
  static String regTelefono = "";
  static String regEmail = "";
  static String regPwd = "";

  static String regSMS = "";
  static String currentIdCliente = "";
  static int counterCarrito = 0;

//--------------------------------------------------------------------------------------------------------------------------------
  //Funciones y metodos genericos
//--------------------------------------------------------------------------------------------------------------------------------
  //Alert generico
  static dialogo(
      {required String titulo, required String mensaje, Function? accion}) {
    Get.dialog(
        AlertDialog(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
          title: Text(titulo),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Text(mensaje),
              SizedBox(
                height: 10,
              ),
              FlutterLogo(
                size: 90.0,
                curve: Curves.bounceIn,
              )
            ],
          ),
          actions: [
            TextButton(
                onPressed: () {
                  Get.back();
                  accion!();
                },
                // style: TextButton.styleFrom(
                //     backgroundColor: Colors.black,
                //     primary: Colors.white,
                //     onSurface: Colors.grey),
                child: Text("Ok"))
          ],
        ),
        barrierDismissible: false);
  }

  static sinAcceso() {
    goToPage(destino: AppRoutes.SINACCESO);
  }

  //GoTo generico sin poder regresar
  static goToPage({required String destino, dynamic? args}) {
    Get.offNamed(destino, arguments: args);
  }

  static TextStyle estiloLabelTextField() {
    return TextStyle(
      color: Colors.black,
      fontSize: 16.0,
      fontWeight: FontWeight.bold,
    );
  }

  static TextStyle estiloLabelAyudaTextField() {
    return TextStyle(
      color: Colors.white,
      fontSize: 16.0,
      fontWeight: FontWeight.bold,
    );
  }

  static TextStyle estiloLabelAvisoPrivacidad() {
    return TextStyle(
      decoration: TextDecoration.underline,
      color: Colors.blue,
      fontSize: 16.0,
      fontWeight: FontWeight.bold,
    );
  }

  static InputDecoration decoracionTextFieldCodigo(
      {required String strhintText}) {
    return InputDecoration(
      isDense: true,
      contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
      enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
          borderRadius: BorderRadius.all(Radius.circular(10.0))),
      focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black, width: 4.0),
          borderRadius: BorderRadius.circular(10.0)),
      hintText: strhintText,
      hintStyle: TextStyle(
        color: Colors.black.withOpacity(.7),
      ),
      errorStyle: TextStyle(
        color: Colors.red,
      ),
    );
  }

  static InputDecoration decoracionTextFieldTelefono(
      {required String strhintText, required String digitos}) {
    return InputDecoration(
      counter: Text("Dígitos $digitos"),
      isDense: true,
      contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
      prefixIcon: Icon(Icons.phone),
      enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
          borderRadius: BorderRadius.all(Radius.circular(10.0))),
      focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black, width: 4.0),
          borderRadius: BorderRadius.circular(10.0)),
      hintText: strhintText,
      //labelText: "Télefono",
      helperText: "Su número es importante para el registro",
      hintStyle: TextStyle(color: Colors.black.withOpacity(.7)),
      errorStyle: TextStyle(
        color: Colors.red,
      ),
    );
  }

  static InputDecoration decoracionTextFieldAddress(
      {required String strhintText}) {
    return InputDecoration(
      isDense: true,
      contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
      prefixIcon: Icon(
        Icons.location_pin,
        size: 24,
        color: Colors.black,
      ),
      // suffixIcon: Icon(
      //   Icons.cancel,
      //   size: 24,
      //   color: Colors.black,
      // ),
      enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
          borderRadius: BorderRadius.all(Radius.circular(10.0))),
      focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black, width: 1.0),
          borderRadius: BorderRadius.circular(10.0)),
      hintText: strhintText,
      hintStyle: TextStyle(
          color: Colors.black.withOpacity(1),
          fontWeight: FontWeight.bold,
          fontSize: 16),
      errorStyle: TextStyle(
        color: Colors.red,
      ),
    );
  }

  static InputDecoration decoracionTextFieldCorreo(
      {required String strhintText,
      required String valido,
      required String helperText}) {
    return InputDecoration(
      counter: Text("$valido"),
      isDense: true,
      contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
      prefixIcon: Icon(Icons.email),
      enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
          borderRadius: BorderRadius.all(Radius.circular(10.0))),
      focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black, width: 4.0),
          borderRadius: BorderRadius.circular(10.0)),
      hintText: strhintText,
      // labelText: "Correo electrónico",
      helperText: helperText, //"Su correo es importante para el registro",
      hintStyle: TextStyle(color: Colors.black.withOpacity(.7)),
      errorStyle: TextStyle(
        color: Colors.red,
      ),
    );
  }

  static InputDecoration decoracionTextFieldPassword(
      {required String strhintText, required String valido}) {
    return InputDecoration(
      counter: Text("$valido"),
      isDense: true,
      contentPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
      prefixIcon: Icon(Icons.lock),
      enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.grey),
          borderRadius: BorderRadius.all(Radius.circular(10.0))),
      focusedBorder: OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black, width: 4.0),
          borderRadius: BorderRadius.circular(10.0)),
      hintText: strhintText,
      // labelText: "Correo electrónico",
      suffixIcon: IconButton(
        icon: Icon(Icons.visibility),
        onPressed: () {
          // Update the state i.e. toogle the state of passwordVisible variable
        },
      ),

      helperText: "Su correo es importante para el registro",
      hintStyle: TextStyle(color: Colors.black.withOpacity(.7)),
      errorStyle: TextStyle(
        color: Colors.red,
      ),
    );
  }

  static Widget cardTipo0() {
    return Card(
      elevation: 10.0,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
      child: Column(
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.photo_album, color: Colors.blue),
            title: Text('Soy el titulo de esta tarjeta'),
            subtitle: Text(
                'Aquí estamos con la descripción de la tajera que quiero que ustedes vean para tener una idea de lo que quiero mostrarles'),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              FlatButton(
                child: Text('Cancelar'),
                onPressed: () {},
              ),
              FlatButton(
                child: Text('Ok'),
                onPressed: () {},
              )
            ],
          )
        ],
      ),
    );
  }

  static Widget cardTipo1({required String texto, required String imagen}) {
    Widget card = GetBuilder<ProductosController>(
      init: ProductosController(),
      builder: (_) {
        return Container(
          child: Column(
            children: [
              FadeInImage.assetNetwork(
                placeholder: "images/loader.gif",
                placeholderScale: 2,
                image: imagen,
                fit: BoxFit.cover,
                //height: 300.0
              ),
              Container(
                padding: EdgeInsets.all(10.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Theme(
                        data: Theme.of(Get.context as BuildContext)
                            .copyWith(canvasColor: Colors.transparent),
                        child: ModalDetalleProducto(
                          texto: texto,
                        )),
                  ],
                ),
              )
            ],
          ),
        );
      },
    );

    return Container(
      child: ClipRRect(child: card, borderRadius: BorderRadius.circular(10.0)),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: <BoxShadow>[
          BoxShadow(
              color: Colors.black26,
              blurRadius: 10.0,
              spreadRadius: 2.0,
              offset: Offset(2.0, 10.0))
        ],
        //color: Colors.black,
        borderRadius: BorderRadius.circular(10.0),
      ),
    );
  }

  static Widget cardTipoCategoria(
      {required String image, required String texto}) {
    return InkWell(
      onTap: () {
        print("Categoria $texto");
      },
      child: Container(
        child: Column(
          children: [
            FadeInImage.assetNetwork(
              placeholder: "images/loader.gif",
              placeholderScale: 2,
              image: image,
              fit: BoxFit.cover,
              height: 48.0,
              width: 48.0,
            ),
            Container(
              padding: EdgeInsets.all(10.0),
              child: Text(texto),
            )
          ],
        ),
      ),
    );
  }

  static Widget lblFormulario({required String etiqueta}) {
    return Padding(
      padding: EdgeInsets.fromLTRB(30.0, 0.0, 30.0, 0.0),
      child: Text(
        etiqueta,
        textAlign: TextAlign.left,
        style: Vuex.estiloLabelTextField(),
      ),
    );
  }
}

// ignore: must_be_immutable
class Cargando extends StatelessWidget {
  double _bottom = 0.0;
  Color _color = Colors.transparent;

  Cargando({double bottom = 300.0, Color color = Colors.transparent}) {
    _bottom = bottom;
    _color = color;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        color: _color,
        width: double.infinity,
        child: Column(mainAxisAlignment: MainAxisAlignment.end, children: [
          CircularProgressIndicator(),
          SizedBox(height: _bottom),
        ]));
  }
}
