import 'package:dio/dio.dart';

class DioBase {
  String _baseUrl = "";

  DioBase({String baseUrl: ""}) {
    _baseUrl = baseUrl;
  }

  Dio dioService() {
    return Dio(BaseOptions(
        connectTimeout: 60000, receiveTimeout: 60000, baseUrl: _baseUrl));
  }

  /* Dio dioMovieService() {
    return Dio(BaseOptions(
        connectTimeout: 5000,
        receiveTimeout: 5000,
        baseUrl: "https://some-website.com"));
  }*/
}

//"https://selfservice-backend.herokuapp.com"
//"https://some-website.com"
