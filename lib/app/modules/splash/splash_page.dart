import 'package:flutter/material.dart';
import 'package:food_trady/app/modules/splash/splash_controller.dart';
import 'package:food_trady/app/utils/vuex.dart';
import 'package:get/get.dart';

class SplashPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<SplashController>(
      builder: (_) {
        return Scaffold(body: Cargando());
      },
    );
  }
}
