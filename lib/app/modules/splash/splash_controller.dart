import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:dio/dio.dart';
import 'package:food_trady/app/data/models/token.dart';
import 'package:food_trady/app/data/repositories/local/local_authentication_repository.dart';
import 'package:food_trady/app/data/repositories/remote/api_repository.dart';
import 'package:food_trady/app/routes/app_routes.dart';
import 'package:food_trady/app/utils/vuex.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class SplashController extends GetxController {
  //busca depenedecia en el entorno que ha sido inyectada en el main
  //DB local
  final LocalAuthRepository _repository = Get.find<LocalAuthRepository>();
  //repository to interact with selfservice api
  final ApiRepository _baseRepository = Get.find<ApiRepository>();

  late Token _myToken;

  @override
  void onReady() {
    super.onReady();
    _init();
  }

  @override
  void onClose() {
    super.onClose();
    //to release resources
  }

  _init() async {
    try {
      //obj to control the back history in android
      BackButtonInterceptor.add(myInterceptor);

      print("on controller splash...");

      bool isRegistered = false;
      bool isAuthenticated = false;

      try {
        isRegistered = await _repository.registrado;
      } catch (ex) {
        isRegistered = false;
      }

      //isRegistered = false;

      try {
        isAuthenticated = await _repository.autenticado;
      } catch (ex) {
        isAuthenticated = false;
      }

      try {
        //obtener token para proximas peticiones
        _myToken = await _baseRepository.newToken(
            identifier: Vuex.identifier, password: Vuex.password);

        print("Token generado");
        print(_myToken.jwt);

        Vuex.myToken = _myToken.jwt;

        //redirije a una pagina dependiendo si esta registrado o no
        if (isRegistered == false) {
          Vuex.goToPage(
              destino: AppRoutes.AYUDAINICIAL); //inicia proceso de registro
        } else if (isAuthenticated) {
          Vuex.goToPage(destino: AppRoutes.HOME); //accede
        } else if (isAuthenticated == false) {
          Vuex.goToPage(destino: AppRoutes.LOGIN); //requiere autenticacion
        }

        //Vuex.goToPage(destino: isRegistered ? AppRoutes.HOME : AppRoutes.AYUDAINICIAL);
      } on DioError catch (ex) {
        if (ex.type == DioErrorType.response) {
          Vuex.dialogo(
              titulo: "ERROR",
              mensaje: "Problemas para generar su token: " + ex.message,
              accion: Vuex.sinAcceso());
        }
      }
    } catch (e) {
      print(e);
      Vuex.dialogo(
          titulo: "ERROR",
          mensaje: "Problemas para ingresar: " + e.toString(),
          accion: Vuex.sinAcceso());
    }
  }

  //Helper to avoid use back button in smartphone
  bool myInterceptor(bool stopDefaultButtonEvent, RouteInfo info) {
    print("BACK BUTTON!");
    return true;
  }
}
