import 'package:flutter/material.dart';
import 'package:food_trady/app/modules/login/login_controller.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class BotonAccion extends StatelessWidget {
  String texto = "";
  //late Function(List<String>) accion;

  BotonAccion({required String texto}) {
    this.texto = texto;
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<LoginController>(builder: (_) {
      return Container(
        padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
        width: double.infinity,
        height: 50.0,
        child: MaterialButton(
          onPressed:
              (_.bCorreoValido.value == false || _.bClaveValido.value == false)
                  ? null
                  : () {
                      // _.imprime("Registro completo!!!!");
                      _.login();
                    },
          child: Text(
            texto,
            style: TextStyle(color: Colors.white),
          ),
          color: Colors.black,
          shape:
              (_.bCorreoValido.value == false || _.bClaveValido.value == false)
                  ? null
                  : RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                      side: BorderSide(color: Colors.black)),
          elevation: 9,
        ),
      );
    });
  }
}
