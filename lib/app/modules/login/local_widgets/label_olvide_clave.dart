import 'package:flutter/material.dart';
import 'package:food_trady/app/utils/vuex.dart';

class LabelOlvideClave extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(30.0, 20.0, 30.0, 15.0),
          child: InkWell(
            child: Text("¿Olvido su contraseña? Reenviar",
                style: Vuex.estiloLabelAvisoPrivacidad()),
            onTap: () {
              print("reenviar contraseña");
            },
          ),
        ),
      ],
    );
  }
}
