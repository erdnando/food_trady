import 'package:flutter/material.dart';

import 'package:food_trady/app/modules/home/local_widgets/detalle_producto/detalle_producto_controller.dart';
import 'package:food_trady/app/routes/app_routes.dart';
import 'package:food_trady/app/utils/vuex.dart';
import 'package:get/get.dart';

class DetalleProductoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<DetalleProductoController>(
      builder: (_) {
        return Scaffold(
            body: Container(
          color: Colors.white,
          width: double.infinity,
          child: Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text("Detalle producto"),
              TextButton(
                  onPressed: () {
                    Vuex.goToPage(destino: AppRoutes.HOME);
                    //Get.back();
                  },
                  child: Text("Regresar"))
            ],
          )),
        ));
      },
    );
  }
}
