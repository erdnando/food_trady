import 'package:food_trady/app/modules/home/local_widgets/detalle_producto/detalle_producto_controller.dart';
import 'package:get/get.dart';

class DetalleProductoBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => DetalleProductoController());
  }
}
