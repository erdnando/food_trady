import 'package:flutter/material.dart';
import 'package:food_trady/app/data/models/categoria.dart';
import 'package:food_trady/app/modules/home/home_controller.dart';

import 'package:food_trady/app/utils/vuex.dart';
import 'package:get/get.dart';

class LstCategorias extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      id: "lstCategorias",
      builder: (_) {
        if (_.loading) {
          return Cargando(bottom: 200.0);
        } else {
          return Expanded(
            child: ListView.builder(
                itemBuilder: (ctx, index) {
                  Categorias c = _.arrCategorias[index];
                  return ListTile(
                    title: new Text(c.descripcionCorta),
                    subtitle: new Text('UUID: ${c.categoriaId}'),
                    leading: new Icon(Icons.map),
                    onTap: () {
                      print(c.descripcionCorta);
                      //Vuex.goToPage(destino: AppRoutes.DETALLECATEGORIA, args: c);
                    },
                  );
                },
                itemCount: _.arrCategorias.length),
          );
        }
      },
    );
  }
}
