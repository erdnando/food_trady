import 'package:flutter/material.dart';
import 'package:food_trady/app/modules/home/home_controller.dart';
import 'package:food_trady/app/modules/home/local_widgets/bottom_bar.dart';
import 'package:get/get.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HomeController>(
      builder: (_) {
        return SafeArea(
          child: Scaffold(
            body: Container(
              color: Colors.white,
              width: double.infinity,
              child: _.showPage,
            ),
            bottomNavigationBar: BottomBar(),
          ),
        );
      },
    );
  }
}
