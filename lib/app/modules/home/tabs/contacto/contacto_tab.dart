import 'package:flutter/material.dart';
import 'package:food_trady/app/data/repositories/local/local_authentication_repository.dart';
import 'package:food_trady/app/modules/home/tabs/contacto/contacto_controller.dart';
import 'package:food_trady/app/routes/app_routes.dart';
import 'package:food_trady/app/utils/vuex.dart';
import 'package:get/get.dart';

class ContactoTab extends StatelessWidget {
  final LocalAuthRepository _localRepository = Get.find<LocalAuthRepository>();

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ContactoController>(
      init: ContactoController(),
      builder: (_) {
        return Container(
          color: Colors.white,
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: Text(_.mensaje),
              ),
              MaterialButton(
                onPressed: () async {
                  print("Cerrando sesion");
                  await _localRepository.setAutenticado(false);
                  Vuex.goToPage(destino: AppRoutes.LOGIN);
                },
                child: Text(
                  'Cerrar sesión',
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.black,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                    side: BorderSide(color: Colors.black)),
                elevation: 9,
              ),
              MaterialButton(
                onPressed: () async {
                  print("Desvincular");
                  await _localRepository.setAutenticado(false);
                  await _localRepository.setRegistrado(false);
                  Vuex.goToPage(destino: AppRoutes.SPLASH);
                },
                child: Text(
                  'Desvincular dispositivo',
                  style: TextStyle(color: Colors.white),
                ),
                color: Colors.black,
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                    side: BorderSide(color: Colors.black)),
                elevation: 9,
              ),
              // MaterialButton(
              //   onPressed: () async {
              //     //final status = await Permission.location.request();
              //     //_.accesoGPS(status);
              //     // _.checkGPSPermission();
              //     // print(status);
              //   },
              //   child: Text(
              //     'Verifica permiso a Location',
              //     style: TextStyle(color: Colors.white),
              //   ),
              //   color: Colors.black,
              //   shape: RoundedRectangleBorder(
              //       borderRadius: BorderRadius.circular(10),
              //       side: BorderSide(color: Colors.black)),
              //   elevation: 9,
              // ),
              // MaterialButton(
              //   onPressed: () async {
              //     //_.accesoGPS(status);
              //     //print(status);
              //     // _.checkGPSEnabled();
              //   },
              //   child: Text(
              //     'Verifica si esta habilitado el GPS',
              //     style: TextStyle(color: Colors.white),
              //   ),
              //   color: Colors.black,
              //   shape: RoundedRectangleBorder(
              //       borderRadius: BorderRadius.circular(10),
              //       side: BorderSide(color: Colors.black)),
              //   elevation: 9,
              // ),
            ],
          ),
        );
      },
    );
  }
}
