import 'package:food_trady/app/utils/vuex.dart';
import 'package:get/get.dart';
// import 'package:permission_handler/permission_handler.dart';
// import 'package:geolocator/geolocator.dart' as Geolocator;

class ContactoController extends GetxController {
  //with WidgetsBindingObserver
  //inyeccion de dependencias
  //repository to interact with selfservice api
  //final FoodTradyRepository _baseRepository = Get.find<FoodTradyRepository>();

  String mensaje = "Contacto";

  bool _loading = true;
  bool get loading => _loading;
  int page = 0;

  @override
  void onReady() {
    super.onReady();
    Vuex.loading = true;

    //WidgetsBinding.instance!.addObserver(this);

    _init();

    update();
  }

  @override
  void onClose() {
    // WidgetsBinding.instance!.removeObserver(this);
    super.onClose();
    //to release resources
  }

//   @override
//   void didChangeAppLifecycleState(AppLifecycleState state) async {
//     print("================> $state");
// //aqui se valida para presentar una opcion para q el usuario pueda brindar permisos y para enterarse si es q el gps esta desactivado
// //con base en el resultado

//     if (state == AppLifecycleState.resumed) {
//       if (!await Permission.location.isGranted) {
//         print("No se dio el permiso Location");
//         //openAppSettings();
//       } else if (!await Geolocator.Geolocator.isLocationServiceEnabled()) {
//         print("El GPS esta desactivado!!! Favor de activarlo");
//       } else {
//         print("Todo OK");
//       }
//     }
//   }

//deprecated
  // void accesoGPS(PermissionStatus status) {
  //   switch (status) {
  //     case PermissionStatus.granted:
  //       print("Ir a una vista");
  //       break;
  //     case PermissionStatus.denied:
  //     case PermissionStatus.restricted:
  //     case PermissionStatus.permanentlyDenied:
  //       openAppSettings();
  //     //break;
  //   }
  // }

//mix de validaciones
  // void checkGPSAndLocation() async {
  //   final permisoGPS = await Permission.location.isGranted;
  //   final gpsActivo = await Geolocator.Geolocator.isLocationServiceEnabled();

  //   //Geolocator.Geolocator.openAppSettings();

  //   if (permisoGPS && gpsActivo) {
  //     print("Se tienen los permisos de location y GPS OK!!!");
  //   } else if (!permisoGPS) {
  //     print("Enviando a una vista donde este el boton de solicitar acceso");
  //   }
  // }

// //llamar para q se le permita al usuario dar permisos d elocaqtion
//   void checkGPSPermission() async {
//     final status = await Permission.location.request();
//     switch (status) {
//       case PermissionStatus.granted:
//         print("Si cuenta con el permiso Location");
//         break;
//       case PermissionStatus.denied:
//       case PermissionStatus.restricted:
//       case PermissionStatus.permanentlyDenied:
//         print("No se dio el permiso Location");
//         openAppSettings();
//       //break;
//     }
//   }

// //para validar si el gps esta activo
//   void checkGPSEnabled() async {
//     final gpsActivo = await Geolocator.Geolocator.isLocationServiceEnabled();

//     if (!gpsActivo) {
//       //generar indicaciobn al usuario de que debe habilitar el GPS
//       print("Active el GPS - Location");
//     } else {
//       print("GPS OK");
//     }
//   }

  _init() async {}
}
