import 'package:flutter/material.dart';

import 'package:food_trady/app/modules/home/tabs/historial/historial_controller.dart';
import 'package:get/get.dart';

class HistorialTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<HistorialController>(
      init: HistorialController(),
      builder: (_) {
        return Container(
          color: Colors.white,
          width: double.infinity,
          child: Center(child: Text(_.mensaje)),
        );
      },
    );
  }
}
