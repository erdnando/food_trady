import 'package:food_trady/app/utils/vuex.dart';
import 'package:get/get.dart';

class CarritoController extends GetxController {
  //inyeccion de dependencias
  //repository to interact with selfservice api
  //final FoodTradyRepository _baseRepository = Get.find<FoodTradyRepository>();

  String mensaje = "Carrito";

  bool _loading = true;
  bool get loading => _loading;
  int page = 0;

  @override
  void onReady() {
    super.onReady();
    Vuex.loading = true;
    _init();

    update();
  }

  @override
  void onClose() {
    super.onClose();
    //to release resources
  }

  _init() async {}
}
