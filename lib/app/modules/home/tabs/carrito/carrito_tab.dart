import 'package:flutter/material.dart';

import 'package:food_trady/app/modules/home/tabs/carrito/carrito_controller.dart';
import 'package:get/get.dart';

class CarritoTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CarritoController>(
      init: CarritoController(),
      builder: (_) {
        return Container(
          color: Colors.white,
          width: double.infinity,
          child: Center(child: Text(_.mensaje)),
        );
      },
    );
  }
}
