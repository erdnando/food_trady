import 'dart:async';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:food_trady/app/utils/vuex.dart';
import 'package:get/get.dart';

class ProductosController extends GetxController {
  //inyeccion de dependencias
  //repository to interact with selfservice api
  //final FoodTradyRepository _baseRepository = Get.find<FoodTradyRepository>();

  String mensaje = "Productos";

  bool _loading = true;
  bool get loading => _loading;
  int page = 0;
  //RxString txtDireccionCliente="".obs;
  final txtDireccionCliente = TextEditingController();

  List<Widget> arrProductosListaPrincipal = [];
  int ultimoItem = 0; //para el paginado de productos de la lista principal
  ScrollController scrollProductosPrincipal = new ScrollController();
  bool isLoadingProductosPrincipal = false;

  @override
  void onReady() {
    super.onReady();
    Vuex.loading = true;
    this.arrProductosListaPrincipal.add(SizedBox(
          height: 10,
        ));
    this.add10ToListaPrincipal();

    scrollProductosPrincipal.addListener(() {
      if (scrollProductosPrincipal.position.pixels ==
          scrollProductosPrincipal.position.maxScrollExtent) {
        fetchData();
        update();
      }
    });
    //Vuex.counterCarrito = 0;
    _init();

    update();
  }

  @override
  void onClose() {
    super.onClose();
    //to release resources
    scrollProductosPrincipal.dispose();
  }

  _init() async {}

  Future fetchData() async {
    this.isLoadingProductosPrincipal = true;
    update();
    final duration = new Duration(seconds: 2);
    return new Timer(duration, respuestaHttp);
  }

  void respuestaHttp() {
    this.isLoadingProductosPrincipal = false;
    scrollProductosPrincipal.animateTo(
        scrollProductosPrincipal.position.pixels + 150,
        duration: Duration(milliseconds: 250),
        curve: Curves.fastOutSlowIn);
    add10ToListaPrincipal();
    //
  }

  getListCategorias(BuildContext context) {
    List<Widget> widgets = [];

    for (int i = 0; i < 10; i++) {
      widgets.add(Padding(
          padding: EdgeInsets.symmetric(horizontal: 15),
          child: InkWell(
            onTap: () {
              print("carrito click");
            },
            child: Container(
              child: Stack(
                clipBehavior: Clip.none,
                children: <Widget>[
                  Icon(Icons.fastfood, size: 40.0, color: Colors.black),
                  Positioned(
                      top: 35.0,
                      right: -11.0,
                      child: Container(
                        alignment: AlignmentDirectional.center,
                        width: 60.0,
                        height: 25.0,
                        child: Text(
                          "Cátegoria",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 12.0,
                              fontWeight: FontWeight.w500),
                        ),
                      )),
                ],
              ),
            ),
          )));
    }

    //widgets.add(Text(this.mensaje));
    // widgets.add(Theme(
    //     data: Theme.of(context).copyWith(canvasColor: Colors.transparent),
    //     child: ModalDetalleProducto()));

    // widgets.add(TextButton(
    //     onPressed: () {
    //       Vuex.goToPage(destino: AppRoutes.DETAIL);
    //     },
    //     child: Text("Detalle")));

    return widgets;
  }

  void add10ToListaPrincipal() {
    for (var i = 0; i < 10; i++) {
      this.ultimoItem++;
      this.arrProductosListaPrincipal.add(Vuex.cardTipo1(
          texto: "¡Lo quiero!",
          imagen: "https://picsum.photos/id/$ultimoItem/1280/720"));

      this.arrProductosListaPrincipal.add(SizedBox(
            height: 20,
          ));
    }

    update();
  }

  showModalAutocompleteddress(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext context) {
          return GetBuilder<ProductosController>(
            init: ProductosController(),
            initState: (_) {},
            builder: (_) {
              return Container(
                padding: EdgeInsets.only(left: 10, right: 10),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    SizedBox(height: 30),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        IconButton(
                          icon: FaIcon(FontAwesomeIcons.arrowLeft,
                              size: 26, color: Colors.black),
                          onPressed: () {
                            update();
                            Navigator.pop(context);
                          },
                        ),
                        Expanded(
                          child: Text(
                            "Direcciones de entrega",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                fontSize: 20, fontWeight: FontWeight.bold),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(height: 0),
                    TextField(
                      controller: txtDireccionCliente,
                      keyboardType: TextInputType.streetAddress,
                      textCapitalization: TextCapitalization.words,
                      autofocus: true,
                      style: TextStyle(fontSize: 22),
                      // onChanged: _.txtTelefonoChanged,
                      onEditingComplete: () {
                        print("termino editar");
                        //_.validarTelefono();
                      },
                      decoration: Vuex.decoracionTextFieldAddress(
                          strhintText: "Ingresa una nueva dirección"),
                    )
                  ],
                ),
                height: MediaQuery.of(context).size.height,
                // width: 30,
                decoration: BoxDecoration(
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.5),
                        spreadRadius: 5,
                        blurRadius: 7,
                        offset: Offset(0, 3), // changes position of shadow
                      ),
                    ],
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20))),
              );
            },
          );
        });
  }

  myShowModalBottomSheet(context) {
    showModalBottomSheet(
        isScrollControlled: true,
        context: context,
        builder: (BuildContext context) {
          return Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Center(child: Text("Detalle del producto")),
                RawMaterialButton(
                    fillColor: Colors.black,
                    constraints: BoxConstraints(minHeight: 50.0, minWidth: 200),
                    elevation: 10,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0)),
                    textStyle: TextStyle(
                        fontSize: 16,
                        fontFamily: 'OpenSans',
                        color: Colors.white,
                        fontWeight: FontWeight.w600),
                    child: Text('Agregar   \$225.00'),
                    onPressed: () {
                      Vuex.counterCarrito++;
                      update();
                      Navigator.pop(context);
                    })
              ],
            ),
            height: MediaQuery.of(context).size.height - 70,
            width: 30,
            decoration: BoxDecoration(
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 5,
                    blurRadius: 7,
                    offset: Offset(0, 3), // changes position of shadow
                  ),
                ],
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20))),
          );
        });
  }
}
