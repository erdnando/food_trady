import 'package:flutter/material.dart';
import 'package:food_trady/app/modules/home/tabs/productos/productos_controller.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class ModalDetalleProducto extends StatelessWidget {
  String texto = "";
  ModalDetalleProducto({required String texto}) {
    this.texto = texto;
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProductosController>(
        init: ProductosController(),
        initState: (_) {},
        builder: (_) {
          return MaterialButton(
            onPressed: () {
              _.myShowModalBottomSheet(context);
            },
            child: Text(
              texto,
              style: TextStyle(color: Colors.white),
            ),
            color: Colors.black,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10),
                side: BorderSide(color: Colors.black)),
            elevation: 9,
          );
        });
  }
}
