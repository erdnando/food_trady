import 'package:flutter/material.dart';
import 'package:food_trady/app/modules/home/tabs/productos/productos_controller.dart';
import 'package:food_trady/app/utils/vuex.dart';
import 'package:get/get.dart';

class AddressFinder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProductosController>(
      init: ProductosController(),
      initState: (_) {},
      builder: (_) {
        return Container(
            height: 40,
            width: MediaQuery.of(context).size.width - 80,
            color: Colors.transparent,
            child: TextField(
              controller: _.txtDireccionCliente,
              enableInteractiveSelection: false,
              style: TextStyle(
                  color: Colors.black.withOpacity(1),
                  fontWeight: FontWeight.bold,
                  fontSize: 16),
              // onEditingComplete: () {
              //   print("termino editar");
              // },
              decoration: Vuex.decoracionTextFieldAddress(
                  strhintText: "Entregar en ..."),
              onTap: () {
                FocusScope.of(context).requestFocus(new FocusNode());
                //TODO: implementar el autocomplementar del domicilio
                _.showModalAutocompleteddress(context);
              },
            ));
      },
    );
  }
}
