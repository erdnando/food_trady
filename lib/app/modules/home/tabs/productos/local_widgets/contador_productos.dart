import 'package:flutter/material.dart';
import 'package:food_trady/app/modules/home/tabs/productos/productos_controller.dart';
import 'package:food_trady/app/utils/vuex.dart';
import 'package:get/get.dart';

class ContadorProductos extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProductosController>(
      init: ProductosController(),
      builder: (_) {
        if (Vuex.counterCarrito > 0) {
          return InkWell(
            onTap: () {
              print("carrito click");
            },
            child: Container(
              child: Stack(
                clipBehavior: Clip.none,
                children: <Widget>[
                  Icon(Icons.shopping_cart_outlined,
                      size: 30.0, color: Colors.black87),
                  Positioned(
                      top: -10.0,
                      right: -14.0,
                      child: Container(
                        alignment: AlignmentDirectional.center,
                        width: 25.0,
                        height: 25.0,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle, color: Colors.red),
                        child: Text(
                          Vuex.counterCarrito.toString(),
                          style: TextStyle(
                              color: Colors.white,
                              fontSize: 15.0,
                              fontWeight: FontWeight.w500),
                        ),
                      )),
                ],
              ),
            ),
          );
        } else {
          return InkWell(
            onTap: () {
              print("carrito click");
            },
            child: Container(
              child: Icon(Icons.shopping_cart, size: 30.0, color: Colors.black),
            ),
          );
        }
      },
    );
  }
}
