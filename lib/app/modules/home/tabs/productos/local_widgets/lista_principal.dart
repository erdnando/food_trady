import 'dart:async';

import 'package:flutter/material.dart';
import 'package:food_trady/app/modules/home/tabs/productos/productos_controller.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class ListaPrincipal extends StatelessWidget {
  ProductosController refLocal = ProductosController();
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProductosController>(
      init: ProductosController(),
      builder: (_) {
        refLocal = _;
        return Expanded(
          child: Stack(
            children: [
              _creaListViewPrincipal(_),
              _crearLoading(_),
            ],
          ),
        );
      },
    );
  }

  Widget _creaListViewPrincipal(ProductosController _) {
    return RefreshIndicator(
      onRefresh: refreshProductos,
      child: ListView.builder(
        controller: _.scrollProductosPrincipal,
        itemCount: _.arrProductosListaPrincipal.length,
        padding: EdgeInsets.only(left: 10, right: 10),
        itemBuilder: (BuildContext context, int index) {
          return _.arrProductosListaPrincipal[index];
        },
      ),
    );
  }

  Future refreshProductos() async {
    final duration = new Duration(seconds: 2);
    new Timer(duration, () {
      refLocal.arrProductosListaPrincipal.clear();
      refLocal.arrProductosListaPrincipal.add(SizedBox(
        height: 10,
      ));
      refLocal.ultimoItem = 0;
      refLocal.add10ToListaPrincipal();
    });

    return Future.delayed(duration);
  }

  Widget _crearLoading(ProductosController _) {
    if (_.isLoadingProductosPrincipal) {
      return Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              CircularProgressIndicator(),
            ],
          ),
          SizedBox(
            height: 15.0,
          )
        ],
      );
    } else {
      return Container();
    }
  }
}
