import 'package:flutter/material.dart';
import 'package:food_trady/app/modules/home/tabs/productos/productos_controller.dart';
import 'package:food_trady/app/utils/vuex.dart';
import 'package:get/get.dart';

class CategoriasList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProductosController>(
      init: ProductosController(),
      builder: (_) {
        return Container(
          height: 90.0,
          child: ListView(scrollDirection: Axis.horizontal, children: [
            SizedBox(
              width: 10,
            ),
            Vuex.cardTipoCategoria(
                image:
                    "https://icons8.com/preloaders/preloaders/326/Pizza%20sliced.gif",
                texto: "Todos"),
            SizedBox(
              width: 0,
            ),
            Vuex.cardTipoCategoria(
                image:
                    "https://icons8.com/preloaders/preloaders/784/Fried%20egg%20animated.gif",
                texto: "Comida"),
            SizedBox(
              width: 0,
            ),
            Vuex.cardTipoCategoria(
                image:
                    "https://icons8.com/preloaders/preloaders/782/Spinning%20cheeseburger.gif",
                texto: "Hamburguesas"),
            SizedBox(
              width: 0,
            ),
            Vuex.cardTipoCategoria(
                image:
                    "https://icons8.com/preloaders/preloaders/783/Hot%20dog%20animated.gif",
                texto: "Hot dogs"),
            SizedBox(
              width: 0,
            ),
            Vuex.cardTipoCategoria(
                image:
                    "https://icons8.com/preloaders/preloaders/469/Apple%20animated.gif",
                texto: "Frutas"),
            SizedBox(
              width: 0,
            ),
            Vuex.cardTipoCategoria(
                image:
                    "https://icons8.com/preloaders/preloaders/470/Banana%20animated.gif",
                texto: "Bananas"),
            SizedBox(
              width: 0,
            ),
            Vuex.cardTipoCategoria(
                image:
                    "https://icons8.com/preloaders/preloaders/475/Tomato%20animated.gif",
                texto: "Tomates"),
          ] //_.getListCategorias(context)
              ),
        );
      },
    );
  }
}
