import 'package:flutter/material.dart';

import 'package:food_trady/app/modules/home/tabs/productos/local_widgets/address_finder.dart';
import 'package:food_trady/app/modules/home/tabs/productos/local_widgets/contador_productos.dart';
import 'package:food_trady/app/modules/home/tabs/productos/productos_controller.dart';

import 'package:get/get.dart';

class HeaderProductos extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProductosController>(
      init: ProductosController(),
      builder: (_) {
        return Padding(
          padding: EdgeInsets.fromLTRB(7.0, 0.0, 10.0, 5.0),
          child: Container(
              alignment: Alignment.center,
              height: 50.0,
              width: double.infinity,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8),
                color: Color(0xc3c7ca).withOpacity(0.3),
              ),
              child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Container(
                      width: 5,
                    ),
                    AddressFinder(),
                    Expanded(child: Container()),
                    ContadorProductos(),
                    Container(
                      width: 15,
                    ),
                  ])),
        );
      },
    );
  }
}
