import 'package:flutter/material.dart';
import 'package:food_trady/app/modules/home/tabs/productos/local_widgets/categoriasList.dart';
import 'package:food_trady/app/modules/home/tabs/productos/local_widgets/header_productos.dart';
import 'package:food_trady/app/modules/home/tabs/productos/local_widgets/lista_principal.dart';
import 'package:food_trady/app/modules/home/tabs/productos/productos_controller.dart';
import 'package:get/get.dart';

class ProductosTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<ProductosController>(
      init: ProductosController(),
      builder: (_) {
        return SafeArea(
          child: Container(
            color: Colors.white,
            width: double.infinity,
            child: Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  height: 10.0,
                ),
                HeaderProductos(),
                CategoriasList(),
                ListaPrincipal(),
              ],
            )),
          ),
        );
      },
    );
  }
}
