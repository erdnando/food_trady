import 'package:flutter/material.dart';
import 'package:food_trady/app/modules/home/tabs/trace/trace_controller.dart';

import 'package:get/get.dart';

class TraceTab extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<TraceController>(
      init: TraceController(),
      builder: (_) {
        return Container(
          color: Colors.white,
          width: double.infinity,
          child: Center(child: Text(_.mensaje)),
        );
      },
    );
  }
}
