import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:geolocator/geolocator.dart' as Geolocator;
import 'package:food_trady/app/utils/vuex.dart';
import 'package:food_trady/app/data/models/categoria.dart';
import 'package:food_trady/app/modules/home/tabs/carrito/carrito_tab.dart';
import 'package:food_trady/app/modules/home/tabs/contacto/contacto_tab.dart';
import 'package:food_trady/app/modules/home/tabs/historial/historial_tab.dart';
import 'package:food_trady/app/modules/home/tabs/productos/productos_tab.dart';
import 'package:food_trady/app/modules/home/tabs/trace/trace_tab.dart';

class HomeController extends GetxController with WidgetsBindingObserver {
  //inyeccion de dependencias
  //repository to interact with selfservice api
  //final FoodTradyRepository _baseRepository = Get.find<FoodTradyRepository>();

  List<Categorias> arrCategorias = [];

  bool _loading = true;
  bool get loading => _loading;
  int tabIndex = 0;
  Widget showPage = ProductosTab();

  @override
  void onReady() {
    super.onReady();
    Vuex.loading = true;
    WidgetsBinding.instance!.addObserver(this);

    _init();
  }

  @override
  void onClose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.onClose();
    //to release resources
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async {
    print("================> $state");
    //aqui se valida para presentar una opcion para q el usuario pueda brindar permisos y para enterarse si es q el gps esta desactivado
    //con base en el resultado

    if (state == AppLifecycleState.resumed) {
      if (!await Permission.location.isGranted) {
        print("No se dio el permiso Location");
        //open popup
        //action:checkGPSPermission;
      } else if (!await Geolocator.Geolocator.isLocationServiceEnabled()) {
        print("El GPS esta desactivado!!! Favor de activarlo");
        //open popup
        //action:Mostrar mensaje de q el GPS esta desactivado;
      } else {
        print("Todo OK");
        //Nothing to do
        await Future.delayed(Duration(seconds: 2));
        this.showModalGPSVerify();
        //
      }
    }
  }

  //llamar para q se le permita al usuario dar permisos d elocaqtion
  void checkGPSPermission() async {
    final status = await Permission.location.request();
    switch (status) {
      case PermissionStatus.granted:
        print("Si cuenta con el permiso Location");
        break;
      case PermissionStatus.denied:
      case PermissionStatus.restricted:
      case PermissionStatus.permanentlyDenied:
        print("No se dio el permiso Location");
        openAppSettings();
      //break;
    }
  }

  //para validar si el gps esta activo
  void checkGPSEnabled() async {
    final gpsActivo = await Geolocator.Geolocator.isLocationServiceEnabled();

    if (!gpsActivo) {
      //generar indicaciobn al usuario de que debe habilitar el GPS
      print("Active el GPS - Location");
    } else {
      print("GPS OK");
    }
  }

  showModalGPSVerify() {
    showModalBottomSheet(
        //isScrollControlled: true,
        context: Get.overlayContext!,
        builder: (BuildContext context) {
          return Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                ListTile(
                  leading: Icon(Icons.photo_album, color: Colors.blue),
                  title: Text('Soy el titulo de esta tarjeta'),
                  subtitle: Text(
                      'Aquí estamos con la descripción de la tajera que quiero que ustedes vean para tener una idea de lo que quiero mostrarles'),
                ),
                Center(child: Text("Detalle del producto")),
                RawMaterialButton(
                    fillColor: Colors.black,
                    constraints: BoxConstraints(minHeight: 50.0, minWidth: 200),
                    elevation: 10,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(8.0)),
                    textStyle: TextStyle(
                        fontSize: 16,
                        fontFamily: 'OpenSans',
                        color: Colors.white,
                        fontWeight: FontWeight.w600),
                    child: Text('Agregar   \$225.00'),
                    onPressed: () {
                      Vuex.counterCarrito++;
                      update();
                      Navigator.pop(context);
                    })
              ],
            ),
            height: 300, //MediaQuery.of(context).size.height - 70,
            width: 30,
            decoration: BoxDecoration(
              boxShadow: [
                BoxShadow(
                  color: Colors.grey.withOpacity(0.5),
                  spreadRadius: 5,
                  blurRadius: 7,
                  offset: Offset(0, 3), // changes position of shadow
                ),
              ],
              color: Colors.white,
              // borderRadius: BorderRadius.only(
              //     topLeft: Radius.circular(20),
              //     topRight: Radius.circular(20))
            ),
          );
        });
  }

  _init() async {}

  void cambiaPagina(int index) {
    this.tabIndex = index;
    showPage = pageChooser(index);
    print(index);
    update();
  }

  Widget pageChooser(int index) {
    switch (index) {
      case 0:
        return ProductosTab();
      case 1:
        return TraceTab();
      case 2:
        return CarritoTab();
      case 3:
        return HistorialTab();
      case 4:
        return ContactoTab();
      default:
        return Container(
          child: Text("Página no encontrada"),
        );
    }
  }
}
