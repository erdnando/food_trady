import 'package:get/get.dart';
import 'verificacion_controller.dart';

class VerificacionBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => VerificacionController());
  }
}
