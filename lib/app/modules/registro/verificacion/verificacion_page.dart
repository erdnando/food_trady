import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:food_trady/app/modules/registro/verificacion/local_widgets/appbar_generic.dart';
import 'package:food_trady/app/modules/registro/verificacion/local_widgets/ayuda.dart';
import 'package:food_trady/app/modules/registro/verificacion/local_widgets/label_reenvio_codigo.dart';
import 'package:food_trady/app/modules/registro/verificacion/local_widgets/txt_codigo_sms.dart';
import 'package:food_trady/app/modules/registro/verificacion/verificacion_controller.dart';
import 'package:food_trady/app/utils/vuex.dart';
import 'package:get/get.dart';

class VerificacionPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    //double anchoDevice = MediaQuery.of(context).size.width;

    return GetBuilder<VerificacionController>(
      builder: (_) {
        if (_.loading) {
          return Cargando(color: Colors.white);
        } else {
          return SafeArea(
            child: Scaffold(

                // resizeToAvoidBottomInset: false,
                appBar: PreferredSize(
                    preferredSize: Size.fromHeight(50.0),
                    child: AppBarGeneric()),
                body: SingleChildScrollView(
                  child: GestureDetector(
                    //para quitar el teclado usar el gesture, focus scope y color transparente al container!!!
                    onTap: () {
                      FocusScope.of(context).unfocus();
                      print("quito teclado");
                      // _.validarTelefono();
                    },
                    child: Container(
                        color: Colors.transparent,
                        width: double.infinity,
                        height: MediaQuery.of(context).size.height - 70,
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(height: 0),
                              CarouselSlider(
                                options: CarouselOptions(
                                  height: 400,
                                  aspectRatio: 16 / 9,
                                  viewportFraction: 0.9,
                                  initialPage: 0,
                                  enableInfiniteScroll: false,
                                  reverse: false,
                                  autoPlay: false,
                                  autoPlayInterval: Duration(seconds: 3),
                                  autoPlayAnimationDuration:
                                      Duration(milliseconds: 800),
                                  autoPlayCurve: Curves.fastOutSlowIn,
                                  enlargeCenterPage: true,
                                  scrollDirection: Axis.horizontal,
                                ),
                                items: _.imageSliders,
                              ),
                              Expanded(child: Container()),
                              Vuex.lblFormulario(
                                etiqueta:
                                    "Hemos enviado un SMS de 6 dígitos a su teléfono, ingreselo para su validación",
                              ),
                              Expanded(child: Container()),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  TxtCodigoSMS(index: 1),
                                  TxtCodigoSMS(index: 2),
                                  TxtCodigoSMS(index: 3),
                                  TxtCodigoSMS(index: 4),
                                  TxtCodigoSMS(index: 5),
                                  TxtCodigoSMS(index: 6),
                                ],
                              ),
                              LabelReenvioCodigo(),
                              Ayuda(),
                              Expanded(child: Container()),
                              //SizedBox(height: 70),
                            ])),
                  ),
                )),
          );
        }
      },
    );
  }
}
