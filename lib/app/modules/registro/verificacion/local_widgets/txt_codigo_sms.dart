import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:food_trady/app/modules/registro/verificacion/verificacion_controller.dart';
import 'package:food_trady/app/utils/vuex.dart';
import 'package:get/get.dart';

// ignore: must_be_immutable
class TxtCodigoSMS extends StatelessWidget {
  int index = 0;
  TxtCodigoSMS({required int index}) {
    this.index = index;
  }

  @override
  Widget build(BuildContext context) {
    return GetBuilder<VerificacionController>(builder: (_) {
      return Container(
        padding: EdgeInsets.fromLTRB(3, 0, 3, 0),
        width: 60,
        child: TextFormField(
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
          onChanged: (valor) {
            _.validarCodigos(index, valor);
          },
          onEditingComplete: () {
            print("termino editar");
            //_.validarTelefono();
          },
          keyboardType: TextInputType.number,
          inputFormatters: <TextInputFormatter>[
            LengthLimitingTextInputFormatter(1),
          ],
          decoration: Vuex.decoracionTextFieldCodigo(strhintText: ""),
        ),
      );
    });
  }
}
