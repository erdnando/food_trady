import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:food_trady/app/routes/app_routes.dart';
import 'package:food_trady/app/utils/vuex.dart';

class AppBarGeneric extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0.0,
      backgroundColor: Colors.white,
      leading: Builder(
        builder: (BuildContext context) {
          return IconButton(
            icon: FaIcon(FontAwesomeIcons.arrowLeft,
                size: 26, color: Colors.black),
            onPressed: () => {Vuex.goToPage(destino: AppRoutes.REGISTRO)},
          );
        },
      ),
      title: Text(''),
      actions: <Widget>[
        IconButton(
          icon: new Icon(Icons.cancel, color: Colors.black, size: 35),
          onPressed: () => print('Salir'),
        ),
      ],
    );
  }
}
