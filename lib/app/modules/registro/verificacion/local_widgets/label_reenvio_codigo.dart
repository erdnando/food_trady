import 'package:flutter/material.dart';
import 'package:food_trady/app/utils/vuex.dart';

class LabelReenvioCodigo extends StatelessWidget {
  const LabelReenvioCodigo({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(30.0, 20.0, 30.0, 15.0),
          child: InkWell(
            child: Text("¿No recibiste el código? Reenviar",
                style: Vuex.estiloLabelAvisoPrivacidad()),
            onTap: () {
              print("reenviar codigo");
            },
          ),
        ),
      ],
    );
  }
}
