import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:food_trady/app/utils/vuex.dart';

class Ayuda extends StatelessWidget {
  const Ayuda({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(30.0, 5.0, 30.0, 10.0),
      child: InkWell(
        onTap: () {
          print("Solicitando ayuda");
        },
        child: Container(
            alignment: Alignment.bottomCenter,
            height: 80.0,
            width: double.infinity,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(19),
              color: Color(0x000000).withOpacity(1),
            ),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  IconButton(
                    icon: Icon(
                      Icons.help,
                      size: 32,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      print("Solicitando ayuda");
                    },
                  ),
                  Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "¿Necesitas ayuda?",
                          textAlign: TextAlign.left,
                          style: Vuex.estiloLabelAyudaTextField(),
                        ),
                        Text(
                          "Da click para ayudarte",
                          textAlign: TextAlign.left,
                          style: Vuex.estiloLabelAyudaTextField(),
                        )
                      ]),
                  IconButton(
                    icon: FaIcon(
                      FontAwesomeIcons.whatsapp,
                      size: 32,
                      color: Colors.white,
                    ),
                    onPressed: () {
                      print("Solicitando ayuda");
                    },
                  ),
                ])),
      ),
    );
  }
}
