import 'package:flutter/material.dart';
import 'package:food_trady/app/data/models/multimedia.dart';
import 'package:food_trady/app/data/repositories/remote/api_repository.dart';
import 'package:food_trady/app/routes/app_routes.dart';
import 'package:food_trady/app/utils/vuex.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class VerificacionController extends GetxController {
  //busca depenedecia en el entorno que ha sido inyectada en el main
  final ApiRepository _baseRepository = Get.find<ApiRepository>();
  late List<Multimedias> _multimedia;
  List<Widget> imageSliders = [Text("")];
  bool _loading = true;
  bool get loading => _loading;

  String code1 = "-1";
  String code2 = "-1";
  String code3 = "-1";
  String code4 = "-1";
  String code5 = "-1";
  String code6 = "-1";

  @override
  void onReady() {
    super.onReady();
    _init();
  }

  @override
  void onClose() {
    super.onClose();
    //to release resources
  }

  void validarCodigos(int index, String valor) async {
    switch (index) {
      case 1:
        this.code1 = valor;
        break;
      case 2:
        this.code2 = valor;
        break;
      case 3:
        this.code3 = valor;
        break;
      case 4:
        this.code4 = valor;
        break;
      case 5:
        this.code5 = valor;
        break;
      case 6:
        this.code6 = valor;
        break;
    }

    update();
    if (this.code1 != "-1" &&
        this.code2 != "-1" &&
        this.code3 != "-1" &&
        this.code4 != "-1" &&
        this.code5 != "-1" &&
        this.code6 != "-1") {
      //TODO: Validate SMS

      Vuex.regSMS = this.code1 +
          this.code2 +
          this.code3 +
          this.code4 +
          this.code5 +
          this.code6;
      await Future.delayed(Duration(seconds: 2));
      Vuex.goToPage(destino: AppRoutes.CORREO);
    }
  }

  _init() async {
    try {
      print("on controller registro...");
      this._loading = true;

      _multimedia = await _baseRepository.multimedias("VERIFICACION");

      imageSliders = _multimedia
          .map((item) => Container(
                child: Container(
                  margin: EdgeInsets.all(22.0),
                  child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                      child: Column(
                        children: <Widget>[
                          // Image.network(item.asset.toString(),
                          //     fit: BoxFit.fill, height: 300.0),
                          FadeInImage.assetNetwork(
                            placeholder: "images/loader.gif",
                            placeholderScale: 2,
                            image: item.asset.toString(),
                            fit: BoxFit.cover,
                            //height: 300.0
                          ),
                          SizedBox(height: 20.0),
                          Text(
                            '${item.descripcion.toString()} ',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      )),
                ),
              ))
          .toList();
      this._loading = false;
      update();
    } catch (e) {
      this._loading = false;
      print(e);
      update();
    }
  }
}
