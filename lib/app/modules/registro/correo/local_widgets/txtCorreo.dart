import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:food_trady/app/modules/registro/correo/correo_controller.dart';
import 'package:food_trady/app/utils/vuex.dart';
import 'package:get/get.dart';

class TxtCorreo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<CorreoController>(builder: (_) {
      return Padding(
        padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 5.0),
        child: TextField(
          maxLength: 60,
          style: TextStyle(fontSize: 22),
          onChanged: _.txtCorreoChanged,
          onEditingComplete: () {
            print("termino editar");
            //_.registraCliente();
          },
          keyboardType: TextInputType.emailAddress,
          inputFormatters: <TextInputFormatter>[
            LengthLimitingTextInputFormatter(50),
          ],
          decoration: Vuex.decoracionTextFieldCorreo(
              strhintText: "mail@domain.com",
              valido: _.bCorreoValido.value == true ? "OK" : "Incompleto",
              helperText: "Su correo es importante para el registro"),
        ),
      );
    });
  }
}
