import 'package:flutter/material.dart';
import 'package:food_trady/app/data/models/cliente-registro.dart';

import 'package:food_trady/app/data/models/multimedia.dart';
import 'package:food_trady/app/data/repositories/local/local_authentication_repository.dart';
import 'package:food_trady/app/data/repositories/remote/api_repository.dart';
import 'package:food_trady/app/routes/app_routes.dart';
import 'package:food_trady/app/utils/vuex.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class CorreoController extends GetxController {
  //busca depenedecia en el entorno que ha sido inyectada en el main
  final ApiRepository _baseRepository = Get.find<ApiRepository>();
  final LocalAuthRepository _localRepository = Get.find<LocalAuthRepository>();
  late List<Multimedias> _multimedia;
  List<Widget> imageSliders = [Text("")];
  bool _loading = true;
  bool get loading => _loading;
  RxString txtTelefono = "".obs;
  RxString txtCorreo = "".obs;
  RxString txtPassword = "".obs;
  List<String> args = ["xxx"];
  RxBool bValido = false.obs;
  RxBool bCorreoValido = false.obs;
  RxBool bClaveValido = false.obs;
  RxBool bVisiblePwd = true.obs;

  @override
  void onReady() {
    super.onReady();
    _init();
  }

  @override
  void onClose() {
    super.onClose();
    //to release resources
  }

  _init() async {
    try {
      print("on controller registro...");
      this._loading = true;

      _multimedia = await _baseRepository.multimedias("CORREO");

      imageSliders = _multimedia
          .map((item) => Container(
                child: Container(
                  margin: EdgeInsets.all(22.0),
                  child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                      child: Column(
                        children: <Widget>[
                          // Image.network(item.asset.toString(),
                          //     fit: BoxFit.fill, height: 300.0),
                          FadeInImage.assetNetwork(
                            placeholder: "images/loader.gif",
                            placeholderScale: 2,
                            image: item.asset.toString(),
                            fit: BoxFit.cover,
                            //height: 300.0
                          ),
                          SizedBox(height: 20.0),
                          Text(
                            '${item.descripcion.toString()}',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      )),
                ),
              ))
          .toList();
      this._loading = false;
      update();
    } catch (e) {
      this._loading = false;
      print(e);
      update();
    }
  }

  void txtCorreoChanged(String value) {
    this.txtCorreo.value = value;

    this.bCorreoValido.value = RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(this.txtCorreo.toString().trim());

    update();
  }

  void txtPasswordChanged(String value) {
    this.txtPassword.value = value;

    if (this.txtPassword.value != "" &&
        txtPassword.value.toString().length == 8) {
      this.bClaveValido.value = true;
    } else {
      this.bClaveValido.value = false;
    }

    update();
  }

  void registraCliente() async {
    //"Valida estructura correo"
    bool emailValid = RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(this.txtCorreo.toString().trim());

    if (this.txtCorreo.toString().trim().length == 0) {
      Vuex.dialogo(titulo: "Aviso", mensaje: "El correo es un campo requerido");
      return;
    } else if (!emailValid) {
      Vuex.dialogo(titulo: "Aviso", mensaje: "Ingrese un correo válido");
      return;
    } else if (this.txtPassword.value.toString().length < 8) {
      Vuex.dialogo(
          titulo: "Aviso",
          mensaje: "Ingrese una contraseña de al menos 8 caracteres");
      return;
    } else {
      Vuex.regEmail = this.txtCorreo.value.toString().trim();
      Vuex.regPwd = this.txtPassword.value.toString().trim();
    }

    this._loading = true;
    update();
    //await Future.delayed(Duration(seconds: 1));
    Vuex.currentIdCliente = "";
    Vuex.cameFromRegister = false;

    ClienteRegistro cliente = await _baseRepository.clienteRegistro(
        correo: Vuex.regEmail, telefono: Vuex.regTelefono, clave: Vuex.regPwd);

    //valida que no este repetido el correo en la db
    if (cliente.existe != "NUEVO") {
      Vuex.dialogo(titulo: "Aviso", mensaje: cliente.mensaje);

      this._loading = false;
      update();
      return;
    } else {
      //Usuario registrado OK
      Vuex.cameFromRegister = true;
      Vuex.currentIdCliente = cliente.mensaje;
      this._loading = false;

      await _localRepository.setRegistrado(true);
      await _localRepository.setAutenticado(true);

      update();
      Vuex.goToPage(destino: AppRoutes.HOME);
    }
  }

  imprime(String args) {
    print(args);
  }

  updateState(String id) {
    update();
  }
}
