import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:food_trady/app/modules/registro/correo/correo_controller.dart';
import 'package:food_trady/app/modules/registro/correo/local_widgets/appbar_generic.dart';
import 'package:food_trady/app/modules/registro/correo/local_widgets/boton_accion.dart';
import 'package:food_trady/app/modules/registro/correo/local_widgets/pwd_correo.dart';
import 'package:food_trady/app/modules/registro/correo/local_widgets/txtCorreo.dart';
import 'package:food_trady/app/utils/vuex.dart';
import 'package:get/get.dart';

class CorreoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // double anchoDevice =
    return GetBuilder<CorreoController>(
      builder: (_) {
        if (_.loading) {
          return Cargando(color: Colors.white);
        } else {
          return SafeArea(
            child: Scaffold(

                // resizeToAvoidBottomInset: false,
                appBar: PreferredSize(
                    preferredSize: Size.fromHeight(50.0),
                    child: AppBarGeneric()),
                body: SingleChildScrollView(
                  child: GestureDetector(
                    //para quitar el teclado usar el gesture, focus scope y color transparente al container!!!
                    onTap: () {
                      FocusScope.of(context).unfocus();
                      print("quito teclado..");
                    },
                    child: Container(
                        color: Colors.transparent,
                        width: double.infinity,
                        height: MediaQuery.of(context).size.height - 70,
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              CarouselSlider(
                                options: CarouselOptions(
                                  height: 400,
                                  aspectRatio: 16 / 9,
                                  viewportFraction: 0.9,
                                  initialPage: 0,
                                  enableInfiniteScroll: false,
                                  reverse: false,
                                  autoPlay: false,
                                  autoPlayInterval: Duration(seconds: 3),
                                  autoPlayAnimationDuration:
                                      Duration(milliseconds: 800),
                                  autoPlayCurve: Curves.fastOutSlowIn,
                                  enlargeCenterPage: true,
                                  scrollDirection: Axis.horizontal,
                                ),
                                items: _.imageSliders,
                              ),
                              Expanded(child: Container()),
                              Vuex.lblFormulario(
                                etiqueta: "Ingrese su correo electrónico",
                              ),
                              TxtCorreo(),
                              Vuex.lblFormulario(
                                etiqueta:
                                    "Escriba su contraseña con la que accederá",
                              ),
                              PwdCorreo(),
                              Vuex.lblFormulario(
                                etiqueta:
                                    "Si no vee nuestros correos, busque en la sección de Spam",
                              ),
                              Expanded(child: Container()),
                              BotonAccion(texto: "¡Listo!"),
                              Expanded(child: Container()),
                              SizedBox(height: 5),
                              Expanded(child: Container()),
                            ])),
                  ),
                )),
          );
        }
      },
    );
  }
}
