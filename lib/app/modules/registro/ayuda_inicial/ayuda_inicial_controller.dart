import 'package:carousel_slider/carousel_slider.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:food_trady/app/data/models/multimedia.dart';
import 'package:food_trady/app/data/repositories/remote/api_repository.dart';
import 'package:food_trady/app/routes/app_routes.dart';
import 'package:food_trady/app/utils/vuex.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class AyudaInicialController extends GetxController {
  //repository to interact with selfservice api
  final ApiRepository _baseRepository = Get.find<ApiRepository>();

  late List<Multimedias> _multimedia;
  bool _loading = true;
  bool get loading => _loading;

  double posicion = 0.0;
  final CarouselController controllerCarousel = CarouselController();
  List<Widget> imageSliders = [Text("")];

  @override
  void onReady() {
    //to do tinghs after render the view
    super.onReady();
    _init();
  }

  @override
  void onClose() {
    super.onClose();
    //to release resources
  }

  _init() async {
    this._loading = true;
    try {
      //obtener token para proximas peticiones
      _multimedia = await _baseRepository.multimedias("AYUDAINICIAL");

      print("Token generado");

      imageSliders = _multimedia
          .map((item) => Container(
                child: Container(
                  margin: EdgeInsets.all(22.0),
                  child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                      child: Column(
                        children: <Widget>[
                          // FadeInImage(
                          //     image: NetworkImage(item.asset.toString()),
                          //     placeholder: AssetImage('images/loader.gif'),
                          //     fadeInDuration: Duration(milliseconds: 200),
                          //     fit: BoxFit.cover,
                          //     height: 300.0),
                          FadeInImage.assetNetwork(
                            placeholder: "images/loader.gif",
                            placeholderScale: 2,
                            image: item.asset.toString(),
                            fit: BoxFit.cover,
                            //height: 300.0
                          ),
                          SizedBox(height: 30.0),
                          Text(
                            '${item.descripcion.toString()} ',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      )),
                ),
              ))
          .toList();

      this._loading = false;
      update();
    } on DioError catch (ex) {
      this._loading = false;
      update();
      if (ex.type == DioErrorType.response) {
        Vuex.dialogo(
            titulo: "ERROR",
            mensaje: "Problemas al realizar la petición: " + ex.message,
            accion: Vuex.sinAcceso());
      }
    }
  }

  Future<void> cambiaPosicion(
      int valor, CarouselPageChangedReason reason) async {
    this.posicion = valor.toDouble();
    print("posicion: $valor");
    update(); //must call this function to update the view
    if (this.posicion + 1 == (_multimedia.length)) {
      await Future.delayed(Duration(seconds: 2));
      Vuex.goToPage(destino: AppRoutes.REGISTRO);
    }
  }
}
