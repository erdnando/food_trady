import 'package:get/get.dart';
import 'ayuda_inicial_controller.dart';

class AyudaInicialBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => AyudaInicialController());
  }
}
