import 'package:carousel_slider/carousel_slider.dart';
import 'package:dots_indicator/dots_indicator.dart';
import 'package:flutter/material.dart';
import 'package:food_trady/app/modules/registro/ayuda_inicial/ayuda_inicial_controller.dart';
import 'package:food_trady/app/utils/vuex.dart';
import 'package:get/get.dart';

class AyudaInicialPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<AyudaInicialController>(
      builder: (_) {
        if (_.loading) {
          return Cargando(color: Colors.white);
        } else {
          return SafeArea(
            child: Scaffold(
                body: Container(
                    width: double.infinity,
                    child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(height: 70),
                          CarouselSlider(
                            options: CarouselOptions(
                              height: 490,
                              aspectRatio: 16 / 9,
                              viewportFraction: 0.9,
                              initialPage: 0,
                              enableInfiniteScroll: false,
                              reverse: false,
                              autoPlay: false,
                              autoPlayInterval: Duration(seconds: 3),
                              autoPlayAnimationDuration:
                                  Duration(milliseconds: 800),
                              autoPlayCurve: Curves.fastOutSlowIn,
                              enlargeCenterPage: true,
                              onPageChanged: _.cambiaPosicion,
                              scrollDirection: Axis.horizontal,
                            ),
                            carouselController: _.controllerCarousel,
                            items: _.imageSliders,
                          ),
                          //SizedBox(height: 70),
                          Expanded(child: Container()),
                          DotsIndicator(
                            onTap: (position) {
                              _.controllerCarousel.jumpToPage(position.toInt());
                              _.cambiaPosicion(position.toInt(),
                                  CarouselPageChangedReason.manual);
                            },
                            dotsCount: _.imageSliders.length,
                            position: _.posicion,
                            decorator: DotsDecorator(
                              size: const Size.square(9.0),
                              spacing: const EdgeInsets.all(10.0),
                              activeSize: const Size(18.0, 9.0),
                              color: Colors.black87, // Inactive color
                              activeColor: Colors.redAccent,
                            ),
                          ),
                          SizedBox(height: 100),
                        ]))),
          );
        }
      },
    );
  }
}
