import 'package:flutter/material.dart';
import 'package:food_trady/app/utils/vuex.dart';

class LabelAvisoPrivacidad extends StatelessWidget {
  const LabelAvisoPrivacidad({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: EdgeInsets.fromLTRB(30.0, 20.0, 30.0, 15.0),
          child: InkWell(
            child: Text("Aviso de privacidad",
                style: Vuex.estiloLabelAvisoPrivacidad()),
            onTap: () {
              print("Show aviso de provacidad");
            },
          ),
        ),
      ],
    );
  }
}
