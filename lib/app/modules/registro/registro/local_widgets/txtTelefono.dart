import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:food_trady/app/modules/registro/registro/registro_controller.dart';
import 'package:food_trady/app/utils/vuex.dart';
import 'package:get/get.dart';

class TxtTelefono extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<RegistroController>(builder: (_) {
      return Padding(
        padding: EdgeInsets.fromLTRB(30.0, 10.0, 30.0, 5.0),
        child: TextField(
          //textCapitalization: TextCapitalization.sentences,
          autofocus: false,
          style: TextStyle(fontSize: 22),
          onChanged: _.txtTelefonoChanged,
          onEditingComplete: () {
            print("termino editar");
            _.validarTelefono();
          },
          keyboardType: TextInputType.phone,
          inputFormatters: <TextInputFormatter>[
            LengthLimitingTextInputFormatter(10),
          ],
          decoration: Vuex.decoracionTextFieldTelefono(
              strhintText: "10 dígitos",
              digitos: _.txtTelefono.value.toString().length.toString()),
        ),
      );
    });
  }
}
