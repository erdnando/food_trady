import 'package:flutter/material.dart';
import 'package:food_trady/app/data/models/cliente-registro.dart';
import 'package:food_trady/app/data/models/multimedia.dart';
import 'package:food_trady/app/data/repositories/remote/api_repository.dart';
import 'package:food_trady/app/routes/app_routes.dart';
import 'package:food_trady/app/utils/vuex.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class RegistroController extends GetxController {
  //busca depenedecia en el entorno que ha sido inyectada en el main
  final ApiRepository _baseRepository = Get.find<ApiRepository>();
  late List<Multimedias> _multimedia;
  List<Widget> imageSliders = [Text("")];
  bool _loading = true;
  bool get loading => _loading;
  RxString txtTelefono = "".obs;

  @override
  void onReady() {
    super.onReady();
    _init();
  }

  @override
  void onClose() {
    super.onClose();
    //to release resources
  }

  _init() async {
    try {
      print("on controller registro...");
      this._loading = true;

      _multimedia = await _baseRepository.multimedias("REGISTRO");

      imageSliders = _multimedia
          .map((item) => Container(
                child: Container(
                  margin: EdgeInsets.all(22.0),
                  child: ClipRRect(
                      borderRadius: BorderRadius.all(Radius.circular(5.0)),
                      child: Column(
                        children: <Widget>[
                          // Image.network(item.asset.toString(),
                          //     fit: BoxFit.fill, height: 300.0),
                          FadeInImage.assetNetwork(
                            placeholder: "images/loader.gif",
                            placeholderScale: 2,
                            image: item.asset.toString(),
                            fit: BoxFit.cover,
                            //height: 300.0
                          ),
                          SizedBox(height: 20.0),
                          Text(
                            '${item.descripcion.toString()} ',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 20.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      )),
                ),
              ))
          .toList();
      this._loading = false;
      update();
    } catch (e) {
      this._loading = false;
      print(e);
      update();
    }
  }

  void txtTelefonoChanged(String value) {
    this.txtTelefono.value = value;
    update();
  }

  void validarTelefono() async {
    print("Validando telefono");
    if (this.txtTelefono.toString().length == 0) {
      return;
    }
    if (this.txtTelefono.toString().length < 10) {
      Vuex.dialogo(
          titulo: "Aviso", mensaje: "El teléfono debe ser de 10 dígitos");
    } else if (this.txtTelefono.toString().length == 10) {
      Vuex.regTelefono = this.txtTelefono.toString().trim();

      ClienteRegistro cliente =
          await _baseRepository.validaTelefono(telefono: Vuex.regTelefono);

      //valida que no este repetido el correo en la db
      if (cliente.existe != "NUEVO") {
        Vuex.dialogo(titulo: "Aviso", mensaje: cliente.mensaje);

        this._loading = false;
        update();
      } else {
        //Usuario registrado OK
        // Vuex.cameFromRegister = true;
        //Vuex.currentIdCliente = cliente.mensaje;
        //this._loading = false;
        update();
        Vuex.goToPage(destino: AppRoutes.VERIFICACION);
      }
      // await Future.delayed(Duration(seconds: 2));

    }
  }
}
