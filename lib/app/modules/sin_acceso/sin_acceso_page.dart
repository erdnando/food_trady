import 'package:flutter/material.dart';
import 'package:food_trady/app/modules/sin_acceso/sin_acceso_controller.dart';
import 'package:food_trady/app/routes/app_routes.dart';
import 'package:get/get.dart';

class SinAccesoPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<SinAccesoController>(
      builder: (_) {
        return Scaffold(
            body: Container(
                width: double.infinity,
                child:
                    Column(mainAxisAlignment: MainAxisAlignment.end, children: [
                  Text("Sin autorización"),
                  SizedBox(height: 150),
                  Text("Su acceso ha caducado"),
                  Text(""),
                  Text("Consulte a su agente"),
                  SizedBox(height: 150),
                  TextButton(
                      onPressed: () {
                        // Vuex.goToPage(AppRoutes.AGENTESDISPONIBLES);
                        Get.offNamed(AppRoutes.SPLASH);
                      },
                      style: TextButton.styleFrom(
                          backgroundColor: Colors.teal,
                          primary: Colors.white,
                          onSurface: Colors.grey),
                      child: Text("Reintentar...")),
                  SizedBox(height: 100),
                ])));
      },
    );
  }
}
