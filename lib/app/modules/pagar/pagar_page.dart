import 'package:flutter/material.dart';
import 'package:food_trady/app/modules/pagar/pagar_controller.dart';
import 'package:food_trady/app/routes/app_routes.dart';
import 'package:food_trady/app/utils/vuex.dart';
import 'package:get/get.dart';

class PagarPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<PagarController>(
      builder: (_) {
        return Scaffold(
            body: Container(
                width: double.infinity,
                child:
                    Column(mainAxisAlignment: MainAxisAlignment.end, children: [
                  Text("PAgar TC"),
                  SizedBox(height: 150),
                  Text("Costo 34,000"),
                  Text("Subtotal 34,000"),
                  Text("Total 34,500"),
                  SizedBox(height: 150),
                  TextButton(
                      onPressed: () {
                        // Vuex.goToPage(LoadingPage()); //no lo continua
                        Vuex.goToPage(
                            destino: AppRoutes
                                .LOADING); //si lo envia pero se auto remueve del stack
                        //Vuex.goToPageRemoveHistory(AppRoutes.LOADING);//si lo envia pero remueve toda la historia
                      },
                      style: TextButton.styleFrom(
                          backgroundColor: Colors.teal,
                          primary: Colors.white,
                          onSurface: Colors.grey),
                      child: Text("A home despues de pagar...")),
                  SizedBox(height: 100),
                ])));
      },
    );
  }
}
