import 'package:flutter/material.dart';
import 'package:food_trady/app/modules/loading/loading_controller.dart';
import 'package:food_trady/app/utils/vuex.dart';
import 'package:get/get.dart';

class LoadingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetBuilder<LoadingController>(
      builder: (_) {
        return Scaffold(body: Cargando());
      },
    );
  }
}
