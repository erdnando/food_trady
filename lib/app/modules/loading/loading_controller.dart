import 'package:food_trady/app/routes/app_routes.dart';
import 'package:food_trady/app/utils/vuex.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/get_state_manager.dart';

class LoadingController extends GetxController {
  //busca depenedecia en el entorno que ha sido inyectada en el main
  //DB local
  // final LocalAuthRepository _repository = Get.find<LocalAuthRepository>();

  @override
  void onReady() {
    // print("on ready...");
    super.onReady();
    _init();
  }

  @override
  void onClose() {
    super.onClose();
    //to release resources
  }

  _init() async {
    try {
      // RequestToken requestToken = await _repository.newRequestToken();
      // Store.TOKEN_GENERADO = requestToken.requestToken;
      print("on controller loading...");
      //como un setTimeOut
      await Future.delayed(Duration(seconds: 2));

      //Vuex.goToPageRef(HomePage());
      Vuex.goToPage(destino: AppRoutes.HOME);
      //Vuex.goToPage(AppRoutes.HOME);
      //Vuex.goToPageRemoveHistory(AppRoutes.HOME);
    } catch (e) {
      print("error en loading...");
      print(e);
    }
  }
}
