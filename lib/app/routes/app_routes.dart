class AppRoutes {
  static const SPLASH = "splash"; //ok
  static const AYUDAINICIAL = "ayuda-inicial"; //ok
  static const REGISTRO = "registro"; //ok
  static const VERIFICACION = "verificacion"; //ok
  static const CORREO = "correo"; //
  static const HOME = "home";
  static const DETAIL = "detail";
  static const PAGAR = "pagar";
  static const LOGIN = "login";
  static const LOADING = "loading";
  static const SINACCESO = "sin-acceso";
}
