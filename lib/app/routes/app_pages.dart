import 'package:food_trady/app/modules/home/home_binding.dart';
import 'package:food_trady/app/modules/home/home_page.dart';
import 'package:food_trady/app/modules/home/local_widgets/detalle_producto/detale_producto_page.dart';
import 'package:food_trady/app/modules/home/local_widgets/detalle_producto/detalle_producto_binding.dart';
import 'package:food_trady/app/modules/loading/loading_binding.dart';
import 'package:food_trady/app/modules/loading/loading_page.dart';
import 'package:food_trady/app/modules/login/login_binding.dart';
import 'package:food_trady/app/modules/login/login_page.dart';
import 'package:food_trady/app/modules/pagar/pagar_binding.dart';
import 'package:food_trady/app/modules/pagar/pagar_page.dart';
import 'package:food_trady/app/modules/registro/ayuda_inicial/ayuda_inicial_binding.dart';
import 'package:food_trady/app/modules/registro/ayuda_inicial/ayuda_inicial_page.dart';
import 'package:food_trady/app/modules/registro/correo/correo_binding.dart';
import 'package:food_trady/app/modules/registro/correo/correo_page.dart';
import 'package:food_trady/app/modules/registro/registro/registro_binding.dart';
import 'package:food_trady/app/modules/registro/registro/registro_page.dart';
import 'package:food_trady/app/modules/registro/verificacion/verificacion_binding.dart';
import 'package:food_trady/app/modules/registro/verificacion/verificacion_page.dart';

import 'package:food_trady/app/modules/sin_acceso/sin_acceso_binding.dart';
import 'package:food_trady/app/modules/sin_acceso/sin_acceso_page.dart';
import 'package:food_trady/app/modules/splash/splash_binding.dart';
import 'package:food_trady/app/modules/splash/splash_page.dart';

import 'package:food_trady/app/routes/app_routes.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';

class AppPages {
  static final List<GetPage> pages = [
    GetPage(
        name: AppRoutes.SPLASH, //ok
        page: () => SplashPage(),
        binding: SplashBinding()),
    GetPage(
        name: AppRoutes.AYUDAINICIAL, //ok
        page: () => AyudaInicialPage(),
        binding: AyudaInicialBinding()),
    GetPage(
        name: AppRoutes.LOGIN,
        page: () => LoginPage(),
        binding: LoginBinding()),
    GetPage(
        name: AppRoutes.REGISTRO, //ok
        page: () => RegistroPage(),
        binding: RegistroBinding()),
    GetPage(
        name: AppRoutes.VERIFICACION, //ok
        page: () => VerificacionPage(),
        binding: VerificacionBinding()),
    GetPage(
        name: AppRoutes.CORREO, //ok
        page: () => CorreoPage(),
        binding: CorreoBinding()),
    GetPage(
        name: AppRoutes.HOME,
        page: () => HomePage(),
        binding: HomeBinding()), //ok
    GetPage(
        name: AppRoutes.DETAIL,
        page: () => DetalleProductoPage(),
        binding: DetalleProductoBinding()),
    GetPage(
        name: AppRoutes.PAGAR,
        page: () => PagarPage(),
        binding: PagarBinding()),
    GetPage(
        name: AppRoutes.LOADING,
        page: () => LoadingPage(),
        binding: LoadingBinding()),
    GetPage(
        name: AppRoutes.SINACCESO,
        page: () => SinAccesoPage(),
        binding: SinAccesoBinding()),
  ];
}
